from ast import literal_eval
import json
import pandas as pd
import numpy as np
from os import listdir
from os.path import isfile, join
import psycopg2
import os
from dotenv import load_dotenv
import psycopg2.extras as extras

HOST = "localhost"
PORT = "5433"
DATABASE = "chatlog_db"
USER = "ducanh"
PASSWORD = "1"
SCHEMA = "fb_chatlog"

param_dict = {
    "host": HOST,
    "port": PORT,
    "database": DATABASE,
    "user": USER,
    "password": PASSWORD
}


def connect_db():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**param_dict)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    print("Connection successful")
    return conn


def select_all(table_name=None):
    # if SCHEMA:
    #     table_name = SCHEMA + '.' + table_name
    conn = connect_db()

    # SQL quert to execute
    # query = "select * from " + table_name
    query = """
    select * from fb_chatlog.raw_chatlog
    where extract(year from created_time) = 2020
    """
    cursor = conn.cursor()
    try:
        cursor.execute(query)
        records = cursor.fetchall()
        col_names = [x.name for x in cursor.description]
        cursor.close()
        return records, col_names
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1


def read_files():
    # file = "data/all_data_31_1.json"
    file = "all_data_31-1.txt"
    contents = []
    with open(file) as f:
        lines = f.readlines()
    #
    # contents = [x.strip() for x in lines if x != "[\n" and x != "]" and len(x) > 1]
    # contents = [x if x[-1] != "," else x[:-1] for x in contents]

    # with open('all_data_31-1.txt', 'w') as f:
    #     for item in contents:
    #         f.write("%s\n" % item)
    contents2 = []
    contents = lines[700000:]
    for i, x in enumerate(contents):
        try:
            x = x.strip()
            if x[-1] == ",":
                x = x[:-1]
            contents2.append(literal_eval(x))
        except:
            a = 0
    # contents = [literal_eval(x) for x in contents]
    df = pd.DataFrame(contents2)

    for col in df.columns:
        if df[col].dtype == object:
            df[col] = df[col].apply(lambda x: np.nan if x == np.nan else str(x).encode('utf-8', 'replace').decode('utf-8'))
    # df.to_csv("data/8.csv", index=False)
    a = 0


def check_df():
    df = pd.read_csv("data/8.csv")
    df2 = pd.read_csv("data/7.csv")
    a = 0


def split_files_to_year():
    year_list = ["2015", "2016", "2017", "2018", "2019", "2020", "2021"]
    for i in range(1, 9):
        files = "data/" + str(i) + ".csv"
        df = pd.read_csv(files)
        for year in year_list:
            sub_df = df[df["created_time"].str.contains(year + "-")]
            output_file = "data/{year}/{year}_{index}.csv"
            output_file = output_file.format(year=year, index=str(i))
            sub_df.to_csv(output_file, index=False)


def combine_year():
    year_list = ["2015", "2016", "2017", "2018", "2019", "2020", "2021"]
    for year in year_list:
        df_list = []
        path = "data/{year}"
        path = path.format(year=year)
        for f in listdir(path):
            df_path = path + "/" + f
            df = pd.read_csv(df_path)
            df_list.append(df)
            a = 0

        df_combine = pd.concat(df_list)
        df_combine.to_csv(path + "/" + year + "_combine.csv", index=False)


def check_combine_year_df():
    year_list = ["2015", "2016", "2017", "2018", "2019", "2020", "2021"]
    # df_list = []
    for year in year_list:
        df_list = []
        df_path = "data/{year}/{year}_combine.csv"
        df_path = df_path.format(year=year)
        df = pd.read_csv(df_path)
        df = df.drop_duplicates(subset=["message_id"])
        fb_conversation_ids = df["fb_conversation_id"].drop_duplicates().to_list()
        for fb_conversation_id in fb_conversation_ids:
            sub_df = df[df["fb_conversation_id"] == fb_conversation_id]
            sub_df = sub_df.sort_values(by=['created_time'])
            df_list.append(sub_df)
        new_df = pd.concat(df_list)
        new_df.to_csv("data/" + year + ".csv", index=False)
    a = 0

    # df = pd.read_csv("data/2020/2020_combine.csv")
    # records, col_names = select_all()
    # df_2 = pd.DataFrame(columns=col_names, data=records)
    # df_2.to_csv("2020.csv", index=False)
    # a = 0
    #
    # list_diff = ['t_172834233910919', 't_3777111159000622', 't_539562903156228']


if __name__ == '__main__':
    # read_files()
    # check_df()
    # split_files_to_year()
    # combine_year()
    check_combine_year_df()
