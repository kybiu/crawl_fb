# Facebook crawl tool
This tool crawls facebook inbox between customer and shop

## Environment variables
Location: fbcrawler/fbcrawler/spiders/.env
````
YEAR: Year to crawl data
THRESHOLD_YEAR: If crawl data from threshold year tool stop

SHOP_NAME: shop name on facebook
ACCESS_TOKEN: facebook page access token
USER_ACCESS_TOKEN: facebook page admin access token (optional)
FANPAGE_ID: shop facebook page id

RETRY_TIMES: number of time the tool sends request to get data when the first time failed
````

## Dependency libraries
`pip install -r requirements`

## Run
#### To run this from console
````
Setup all environment variables
command: scrapy crawl fb_spider -o fbcrawler/data/kscloset/data.json -t json
         python fbcrawler/split_chatlog_to_month.py to split json file into months
````

## Inbox Data description
|Columns   	          |  Description 	                                                                      | 
|---	              |---	                                                                              |
| message_id  	      |id tin nhắn , mỗi một tin nhắn được gửi đi sẽ có 1 mã id riêng biệt , 1 khách nhắn 5 mess với shop sẽ có 5 id khác nhau.	                  |
| fb_conversation_id  |id của tin nhắn giữa shop và khách. 1 nick FB nhắn với shop sẽ có 1 mã fb_conversation_id   	      |
| sender_id	          |id của người gửi    |
| to_sender_id (sau sẽ chuyển thành receiver_id)	|id người nhận  (Mỗi một nick FB sẽ có 1 sender id) |
| sender_name	      |tên người gửi message |
| user_message	      |tin nhắn khách gửi cho shop |
| shop_message	      |tin nhắn shop trả lời khách |
| updated_time	      | thời gian của tin nhắn mới nhất của conversation giữa khách hàng với shop |
| created_time	      |thời gian khi tin nhắn được gửi |
| attachments	      |ảnh mà khách hoặc shop gửi |
| link (link này đang ko hoạt động)	|Link đến inbox của khách |


## Comment Data description
|Columns   	          |  Description 	                                                                      | 
|---	              |---	                                                                              |
| comment_id  	      |Mỗi comment trên facebook sẽ có 1 unique id|
| parent_comment_id  |Id của comment gốc    	      |
| post_id	          |id bài post    |
| comment_message	|Nội dung của comment |
| created_time	      |Thời gian comment được tạo |
| sender_id	      |id người viết comment |
| sender_name	      |tên người viêt comment |
| attachments	      |link ảnh gửi trong comment|
| reaction	      |reaction của comment |
| reaction_count	      |số lượng reaction mà comment có |
| link_to_comment	|Link đến comment của khách |



## Post Data description
|Columns   	          |  Description 	                                                                      | 
|---	              |---	                                                                              |
| post_id  	      |Mỗi post trên facebook sẽ có 1 unique id|
| post_message  |Nội dung của vài post    	      |
| created_time	          |Thời gian bài viết được tạo    |
| updated_time	||
| comment_count	      |Số comment mà bài post có |
| attachments	      |link ảnh có trong bài post |
| attachments_count	      |số ảnh trong bài post |
| reaction	      |reaction của bài post|
| reaction_count	      |số reaction của bài post|
| link_to_post	      |link đến bài post |

