import logging
import scrapy
import json
from datetime import date
from csv import DictWriter
import time
import datetime
import os
from dotenv import load_dotenv
from collections import Iterable
import pandas as pd
import sys
from fbcrawler.spiders.db_connector import insert_to_db, get_post_id_by_time_range, get_video_id_by_time_range

load_dotenv()

sys.path.append('.')
sys.path.append('../')

logging.basicConfig(
    filename='error_id.log',
    level=logging.CRITICAL,
    format='%(asctime)s %(module)s - %(funcName)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)

FANPAGE_ID = os.environ.get("FANPAGE_ID")
ACCESS_TOKEN = os.environ.get("ACCESS_TOKEN")
RETRY_TIMES = os.environ.get("RETRY_TIMES")
reaction_dict = {"NONE": 1, "LIKE": 2, "LOVE": 3, "WOW": 4, "HAHA": 5, "SORRY": 6, "ANGRY": 7, "CARE": 8, "SAD": 9}


def get_timestamp(timestamp: int, format: str):
    """

    :param timestamp:
    :param format: %Y-%m-%d %H:%M:%S
    :return:
    """
    readable_timestamp = datetime.datetime.utcfromtimestamp(timestamp).strftime(format)
    return readable_timestamp


def get_reaction_id(reaction_type: str):
    return reaction_dict[reaction_type]


def flatten(lis):
    for item in lis:
        if isinstance(item, list):
            for x in flatten(item):
                yield x
        else:
            yield item


def convert_time_to_correct_timezone(message_time: str):
    message_time = message_time.replace("T", " ")
    message_time = message_time.replace("+0000", " ")
    message_time = message_time.strip()
    message_time = datetime.datetime.strptime(message_time, "%Y-%m-%d %H:%M:%S")

    hours_added = datetime.timedelta(hours=7)
    message_time = message_time + hours_added
    message_time = str(message_time)
    return message_time


def readable_api_url(api_url: str):
    new_api_url = api_url.replace("%7B", "{{")
    new_api_url = new_api_url.replace("%7D", "}}")
    new_api_url = new_api_url.replace("%2C", ",")
    return new_api_url


def update_comment_reaction_table(comment_reaction, comment_id):
    for index, item in enumerate(comment_reaction):
        item_data = item["data"]
        reaction_type = [x["type"] for x in item_data]


class FbCommentSpider(scrapy.Spider):
    name = 'fb_comment_spider_2'

    post_data = get_post_id_by_time_range(start='2020-01-01', end='2021-01-01', shop_id=FANPAGE_ID)
    post_data = post_data[0]

    video_data = get_video_id_by_time_range(start='2020-01-01', end='2021-01-01', shop_id=FANPAGE_ID)
    video_data = video_data[0]

    all_parent_id = []
    all_post_id = []
    number_of_all_post_id = []

    def start_requests(self):
        logging.critical("Start request")
        for post_ids in self.post_data:
            post_ids_combine = post_ids[0] + "," + post_ids[1]
            post_ids_combine = post_ids_combine.split(",")
            post_ids_combine = [x for x in post_ids_combine if x != ""]
            self.number_of_all_post_id += post_ids_combine
            for post_id in post_ids_combine:
                comment_api = "https://graph.facebook.com/v9.0/{post_id}?fields=comments{{id,from,created_time,message,permalink_url,reactions{{id,name,type}},comments{{id,from,created_time,message,permalink_url,reactions{{id,name,type}},message_tags,parent}},parent}}&access_token={token}"
                comment_api_url = comment_api.format(post_id=post_id, token=ACCESS_TOKEN)
                yield scrapy.Request(url=comment_api_url, callback=self.parse_comment, cb_kwargs={'parent_post_id': post_ids_combine[0], 'post_id': post_id})

        # for video_id in self.video_data:
        #     video_id = video_id[0]
        #     comment_api = "https://graph.facebook.com/v9.0/{video_id}?fields=comments{{id,from,created_time,message,permalink_url,reactions{{id,name,type}},comments{{id,from,created_time,message,permalink_url,reactions{{id,name,type}},message_tags,parent}},parent}}&access_token={token}"
        #     comment_api_url = comment_api.format(video_id=video_id, token=ACCESS_TOKEN)
        #     yield scrapy.Request(url=comment_api_url, callback=self.parse_comment, cb_kwargs={'parent_post_id': str(video_id), 'post_id': str(video_id)})

    def parse_comment(self, response, parent_post_id, post_id):

        logging.critical("Parse")
        jsonresponse = json.loads(response.text)
        if "comments" in jsonresponse or "data" in jsonresponse:
            try:
                comments = jsonresponse["comments"]
            except:
                comments = jsonresponse

            comment_data = comments["data"]

            # comment
            comment_id = [x["id"] for x in comment_data]
            comment_sender_id = [int(x["from"]["id"]) for x in comment_data]
            comment_sender_name = [x["from"]["name"] for x in comment_data]
            comment_created_time = list(map(convert_time_to_correct_timezone, [x["created_time"] for x in comment_data]))
            comment_message = [x["message"] for x in comment_data]
            comment_link = [x["permalink_url"] for x in comment_data]

            comment_reaction = [x["reactions"] if "reactions" in x else [] for x in comment_data]
            # update_comment_reaction_table(comment_reaction, comment_id)
            comment_reaction_count = [len(x["data"]) if x != [] else 0 for x in comment_reaction]
            comment_reaction_data_json = [json.dumps(x["data"]) if x != [] else '' for x in comment_reaction]

            comment_attachments = [x["attachments"] if "attachments" in x else [] for x in comment_data]
            comment_attachments_data_json = [json.dumps(x["data"]) if x != [] else '' for x in comment_attachments]

            comment_parent_id = [None] * len(comment_id)

            # child comment
            child_comments = [x["comments"] for x in comment_data if "comments" in x]
            child_comments_reply = [x["data"] for x in child_comments]
            child_comments_data = list(flatten(child_comments_reply))

            child_comment_id = [x["id"] for x in child_comments_data]
            child_comment_sender_id = [int(x["from"]["id"]) for x in child_comments_data]
            child_comment_sender_name = [x["from"]["name"] for x in child_comments_data]
            child_comment_created_time = list(map(convert_time_to_correct_timezone, [x["created_time"] for x in child_comments_data]))
            child_comment_message = [x["message"] for x in child_comments_data]
            child_comment_link = [x["permalink_url"] for x in child_comments_data]
            child_comment_parent_id = [x["parent"]["id"] for x in child_comments_data]

            child_comment_reaction = [x["reactions"] if "reactions" in x else [] for x in child_comments_data]
            child_comment_reaction_count = [len(x["data"]) if x != [] else 0 for x in child_comment_reaction]
            child_comment_reaction_data_json = [json.dumps(x["data"]) if x != [] else '' for x in child_comment_reaction]

            child_comment_attachments = [x["attachments"] if "attachments" in x else [] for x in child_comments_data]
            child_comment_attachments_data_json = [json.dumps(x["data"]) if x != [] else '' for x in child_comment_attachments]

            # merge comment and child comment (data for comment table)
            all_comment_id = comment_id + child_comment_id
            all_comment_sender_id = comment_sender_id + child_comment_sender_id
            all_comment_sender_name = comment_sender_name + child_comment_sender_name
            all_comment_created_time = comment_created_time + child_comment_created_time
            all_comment_message = comment_message + child_comment_message
            all_comment_link = comment_link + child_comment_link
            all_comment_parent_id = comment_parent_id + child_comment_parent_id
            all_comment_reaction_json = comment_reaction_data_json + child_comment_reaction_data_json
            all_comment_reaction_count = comment_reaction_count + child_comment_reaction_count
            all_comment_attachment_json = comment_attachments_data_json + child_comment_attachments_data_json

            comment_df = pd.DataFrame(
                {'comment_id': all_comment_id,
                 'parent_comment_id': all_comment_parent_id,
                 'parent_post_id': [parent_post_id] * len(all_comment_parent_id),
                 'post_id': [post_id] * len(all_comment_parent_id),
                 'comment_message': all_comment_message,
                 'created_time': all_comment_created_time,
                 'sender_id': all_comment_sender_id,
                 'sender_name': all_comment_sender_name,
                 'attachments': all_comment_attachment_json,
                 'reaction': all_comment_reaction_json,
                 'reaction_count': all_comment_reaction_count,
                 'link_to_comment': all_comment_link,
                 'shop_id': [FANPAGE_ID] * len(all_comment_parent_id),
                 })

            insert_to_db(df=comment_df, table_name='comment')

            if "next" in comments["paging"]:
                logging.warning("Continue parse")
                next = comments["paging"]["next"]
                yield scrapy.Request(next, callback=self.parse_comment, cb_kwargs={'parent_post_id': parent_post_id, 'post_id': next.split("/")[4]})
