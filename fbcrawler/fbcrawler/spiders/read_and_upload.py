import pandas as pd
import ast
import json
import psycopg2
import os
from dotenv import load_dotenv
import psycopg2.extras as extras

load_dotenv()
HOST = os.environ.get("HOST")
PORT = os.environ.get("PORT")
DATABASE = os.environ.get("DATABASE")
USER = os.environ.get("USER")
PASSWORD = os.environ.get("PASSWORD")
SCHEMA = os.environ.get("SCHEMA")

param_dict = {
    "host": HOST,
    "port": PORT,
    "database": DATABASE,
    "user": USER,
    "password": PASSWORD
}
raw_chatlog_table = "raw_chatlog"
if SCHEMA:
    raw_chatlog_table = SCHEMA + "." + raw_chatlog_table


def read_data_from_json():
    with open('page_chinh/2020/24_12/data.json', encoding='utf-8') as data_file:
        data = json.loads(data_file.read())

    df = pd.DataFrame(data)
    df.rename(columns={"to_sender_id": "receiver_id"}, inplace=True)
    return df


def connect_db():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**param_dict)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    print("Connection successful")
    return conn


def insert_to_db(df: pd.DataFrame, table_name):
    """
       Using psycopg2.extras.execute_values() to insert the dataframe
    """
    conn = connect_db()
    # Create a list of tupples from the dataframe values
    tuples = [tuple(x) for x in df.to_numpy()]

    # Comma-separated dataframe columns
    cols = ','.join(list(df.columns))

    # SQL quert to execute
    query = "INSERT INTO %s(%s) VALUES %%s" % (table_name, cols)
    cursor = conn.cursor()
    try:
        extras.execute_values(cursor, query, tuples)
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1
    print("execute_values() done")
    cursor.close()


def main():
    df = read_data_from_json()
    insert_to_db(df, table_name=raw_chatlog_table)

if __name__ == '__main__':
    main()