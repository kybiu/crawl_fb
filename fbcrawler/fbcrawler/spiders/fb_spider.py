import logging
import scrapy
import json
from datetime import date
from csv import DictWriter
import time
import datetime
import os
from dotenv import load_dotenv
from scrapy.spidermiddlewares.httperror import HttpError
from twisted.internet.error import DNSLookupError
from twisted.internet.error import TimeoutError, TCPTimedOutError
import pandas as pd
from fbcrawler.spiders.db_connector import insert_to_db

load_dotenv()

logging.basicConfig(
    filename='error_id.log',
    level=logging.WARNING,
    format='%(asctime)s %(module)s - %(funcName)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)
# logging.warning('And this, too')

FANPAGE_ID = os.environ.get("FANPAGE_ID")
ACCESS_TOKEN = os.environ.get("ACCESS_TOKEN")
RETRY_TIMES = os.environ.get("RETRY_TIMES")

meta = {'handle_httpstatus_all': True}


def get_timestamp(timestamp: int, format: str):
    """

    :param timestamp:
    :param format: %Y-%m-%d %H:%M:%S
    :return:
    """
    readable_timestamp = datetime.datetime.utcfromtimestamp(timestamp).strftime(format)
    return readable_timestamp


class FbSpiderSpider(scrapy.Spider):
    name = 'fb_spider'
    all_conversation_data = []
    handle_httpstatus_list = [400]

    conversation_api = "https://graph.facebook.com/v9.0/{fanpage_id}/conversations?access_token={token}"
    conversation_api = conversation_api.format(fanpage_id=FANPAGE_ID, token=ACCESS_TOKEN)

    conversation_message_api = "https://graph.facebook.com/v9.0/{id}/messages?fields=from,to,message,tags,created_time,attachments,share,sticker&access_token={token}"
    conversation_timestamp_year = os.getenv("YEAR")


    def start_requests(self):
        logging.critical("Start request")
        url = self.conversation_api
        yield scrapy.Request(url=url, callback=self.parse_conversation, errback=self.errback_httpbin, meta=meta,
                             cb_kwargs={'request_url': url, 'retry_time': 0})

    def parse_conversation(self, response, request_url, retry_time):
        logging.critical("Parse")
        if response.status != 200:
            print("Status: " + str(response.status))
            print("URL: " + str(request_url))
            print(retry_time)
            if retry_time < 10:
                retry_time += 1
                yield scrapy.Request(url=request_url, callback=self.parse_conversation, errback=self.errback_httpbin, meta=meta, dont_filter = True,
                                     cb_kwargs={'request_url': request_url, 'retry_time': retry_time})

            else:
                print("Give up sending request")

        jsonresponse = json.loads(response.text)

        try:
            conversations = jsonresponse["conversations"]
        except:
            conversations = jsonresponse

        if "data" not in conversations and "paging" not in conversations:
            return
        conversation_data = conversations["data"]

        if "next" in conversations["paging"]:
            next = conversations["paging"]["next"]
        else:
            next = False

        updated_time = False
        if len(conversation_data) > 0:
            first_item = conversation_data[0]
            updated_time = first_item["updated_time"]
            updated_time = updated_time.replace("T", " ")
            updated_time = updated_time.replace("+0000", " ")
            updated_time = updated_time.strip()
            updated_time = datetime.datetime.strptime(updated_time, "%Y-%m-%d %H:%M:%S")

            hours_added = datetime.timedelta(hours=7)
            updated_time = updated_time + hours_added
            updated_time = str(updated_time)

        stop = False

        if updated_time and int(updated_time[:4]) >= int(os.getenv("YEAR")):
            self.all_conversation_data += conversation_data
            logging.warning("Continue parse")
            if next:
                yield scrapy.Request(next, callback=self.parse_conversation, errback=self.errback_httpbin, meta=meta,
                                     cb_kwargs={'request_url': next, 'retry_time': 0})
            else:
                stop = True
        else:
            stop = True

        if updated_time and stop:
            with open("id_to_crawl_again", "w") as text_file:
                text_file.write("%s \n" % str(len(self.all_conversation_data)))

            for item in self.all_conversation_data:
                id = item["id"]
                with open("id_to_crawl_again", "w") as text_file:
                    text_file.write("%s \n" % str(id))
                link = item["link"]
                updated_time = item["updated_time"]

                updated_time = updated_time.replace("T", " ")
                updated_time = updated_time.replace("+0000", " ")
                updated_time = updated_time.strip()
                updated_time = datetime.datetime.strptime(updated_time, "%Y-%m-%d %H:%M:%S")

                hours_added = datetime.timedelta(hours=7)
                updated_time = updated_time + hours_added
                updated_time = str(updated_time)

                url = self.conversation_message_api.format(id=id, token=ACCESS_TOKEN)
                yield scrapy.Request(url, callback=self.parse_conversation_message, errback=self.errback_httpbin,
                                     meta=meta,
                                     cb_kwargs={'id': id, 'conversation_link': link, 'updated_time': updated_time,
                                                'retry_times': 0})

    def parse_conversation_message(self, response, id, conversation_link, updated_time, retry_times):
        logging.warning("Parse conversation message")
        data = []
        next = None

        try:
            jsonresponse = json.loads(response.text)
            if "paging" in jsonresponse and "next" in jsonresponse["paging"]:
                next = jsonresponse["paging"]["next"]
            data = jsonresponse["data"]
        except:
            if retry_times < RETRY_TIMES:
                retry_times += 1
                url = self.conversation_message_api.format(id=id, token=ACCESS_TOKEN)
                yield scrapy.Request(url, callback=self.parse_conversation_message, errback=self.errback_httpbin,
                                     meta=meta,
                                     cb_kwargs={'id': id, 'conversation_link': conversation_link,
                                                'updated_time': updated_time, 'retry_times': retry_times})
            else:
                # logging.critical("error_id: " + str(id))
                with open("id_to_crawl_again", "w") as text_file:
                    text_file.write("%s" % str(id))
                yield {
                    'shop_id': "",
                    'message_id': "",
                    'fb_conversation_id': "",
                    'sender_id': "",
                    'to_sender_id': "",
                    'sender_name': "",
                    'user_message': "",
                    'shop_message': "",
                    'updated_time': "",
                    'created_time': "",
                    'attachments': "",
                    'sticker': "",
                    'link': ""
                }

        shop_name = os.environ.get("SHOP_NAME")
        # shop_name = "Shop Gấu & Bí Ngô - Đồ dùng Mẹ & Bé cao cấp"
        if data:
            for item in data:
                created_time = item["created_time"]
                if isinstance(created_time, str):
                    created_time = created_time.replace("T", " ")
                    created_time = created_time.replace("+0000", " ")
                    created_time = created_time.strip()
                    created_time = datetime.datetime.strptime(created_time, "%Y-%m-%d %H:%M:%S")

                    # Cong them 7 tieng vi VN hon gio quoc te 7 tieng
                    hours_added = datetime.timedelta(hours=7)
                    created_time = created_time + hours_added
                    created_time = str(created_time)

                # year_created = created_time[:4]
                # if year_created == self.conversation_timestamp_year:
                message_from = item["from"]["name"]
                message_id = item["id"]

                # if "2020-12" in created_time:
                if created_time:
                    created_time_date = time.mktime(datetime.datetime.strptime(created_time[:10], "%Y-%m-%d").timetuple())
                    created_time_month = get_timestamp(int(created_time_date), "%m")
                    if created_time_month:
                        message_attachments = ""
                        if 'shares' in item:
                            if "data" in item["shares"]:
                                if any(x in item["shares"]["data"][0] for x in
                                       ["description", "link", "name", "template"]):
                                    a = 0
                        if 'attachments' in item:
                            attachments_data = item["attachments"]["data"]
                            try:
                                for attachment in attachments_data:
                                    message_attachments += attachment["image_data"]["url"] + ", "
                            except:
                                message_attachments = " "

                        if "sticker" in item:
                            sticker_value = item["sticker"]
                        else:
                            sticker_value = ""

                        sender_id = ""
                        to_sender_id = ""
                        if "id" in item["from"]:
                            sender_id = item["from"]["id"]
                        if "data" in item["to"] and len(item["to"]["data"]) > 0:
                            to_sender_id = item["to"]["data"][0]["id"]

                        sent_message = item["message"].encode('utf-16', 'surrogatepass').decode('utf-16')
                        if message_from != shop_name:
                            user_message = sent_message
                            shop_message = ""
                        else:
                            user_message = ""
                            shop_message = sent_message

                        # yield {
                        #     'shop_id': FANPAGE_ID,
                        #     'message_id': message_id,
                        #     'fb_conversation_id': id,
                        #     'sender_id': sender_id,
                        #     'to_sender_id': to_sender_id,
                        #     'sender_name': message_from,
                        #     'user_message': user_message,
                        #     'shop_message': shop_message,
                        #     'updated_time': updated_time,
                        #     'created_time': created_time,
                        #     'attachments': message_attachments,
                        #     'sticker': sticker_value,
                        #     'link': conversation_link
                        # }
                        raw_chatlog_df = pd.DataFrame(
                            {
                                'shop_id': [FANPAGE_ID],
                                'message_id': [message_id],
                                'fb_conversation_id': [id],
                                'sender_id': [sender_id],
                                'receiver_id': [to_sender_id],
                                'sender_name': [message_from],
                                'user_message': [user_message],
                                'shop_message': [shop_message],
                                'updated_time': [updated_time],
                                'created_time': [created_time],
                                'attachments': [message_attachments],
                                'sticker': [sticker_value],
                                'link': [conversation_link]
                            }
                        )

                        insert_to_db(df=raw_chatlog_df, table_name='raw_chatlog')

            if next:
                yield scrapy.Request(url=next, callback=self.parse_conversation_message, errback=self.errback_httpbin,
                                     meta=meta,
                                     cb_kwargs={'id': id, 'conversation_link': conversation_link,
                                                'updated_time': updated_time, 'retry_times': 0})

    def errback_httpbin(self, failure):
        # log all failures
        self.logger.error(repr(failure))

        # in case you want to do something special for some errors,
        # you may need the failure's type:

        if failure.check(HttpError):
            # these exceptions come from HttpError spider middleware
            # you can get the non-200 response
            response = failure.value.response
            self.logger.error('HttpError on %s', response.url)

        elif failure.check(DNSLookupError):
            # this is the original request
            request = failure.request
            self.logger.error('DNSLookupError on %s', request.url)

        elif failure.check(TimeoutError, TCPTimedOutError):
            request = failure.request
            self.logger.error('TimeoutError on %s', request.url)
