import logging
import scrapy
import json
from datetime import date
from csv import DictWriter
import time
import datetime
import os
from dotenv import load_dotenv
from collections import Iterable
import pandas as pd
import sys
from fbcrawler.spiders.db_connector import insert_to_db

load_dotenv()

sys.path.append('.')
sys.path.append('../')

logging.basicConfig(
    filename='error_id.log',
    level=logging.CRITICAL,
    format='%(asctime)s %(module)s - %(funcName)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)

FANPAGE_ID = os.environ.get("FANPAGE_ID")
ACCESS_TOKEN = os.environ.get("ACCESS_TOKEN")
RETRY_TIMES = os.environ.get("RETRY_TIMES")
reaction_dict = {"NONE": 1, "LIKE": 2, "LOVE": 3, "WOW": 4, "HAHA": 5, "SORRY": 6, "ANGRY": 7, "CARE": 8, "SAD": 9}


def get_timestamp(timestamp: int, format: str):
    """

    :param timestamp:
    :param format: %Y-%m-%d %H:%M:%S
    :return:
    """
    readable_timestamp = datetime.datetime.utcfromtimestamp(timestamp).strftime(format)
    return readable_timestamp


def get_reaction_id(reaction_type: str):
    return reaction_dict[reaction_type]


def convert_time_to_correct_timezone(message_time: str):
    message_time = message_time.replace("T", " ")
    message_time = message_time.replace("+0000", " ")
    message_time = message_time.strip()
    message_time = datetime.datetime.strptime(message_time, "%Y-%m-%d %H:%M:%S")

    hours_added = datetime.timedelta(hours=7)
    message_time = message_time + hours_added
    message_time = str(message_time)
    return message_time


def update_post_reaction_table():
    pass


def readable_api_url(api_url: str):
    new_api_url = api_url.replace("%7B", "{{")
    new_api_url = new_api_url.replace("%7D", "}}")
    new_api_url = new_api_url.replace("%2C", ",")
    return new_api_url


class FbVideoSpider(scrapy.Spider):
    name = 'fb_video_spider'

    video_api = "https://graph.facebook.com/v9.0/{fanpage_id}?fields=videos{{id,permalink_url,title,description,created_time,updated_time}}&access_token={token}"
    video_api = video_api.format(fanpage_id=FANPAGE_ID, token=ACCESS_TOKEN)

    conversation_timestamp_year = os.getenv("YEAR")

    def start_requests(self):
        logging.critical("Start request")
        url = self.video_api
        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        logging.critical("Parse")
        jsonresponse = json.loads(response.text)
        try:
            videos = jsonresponse["videos"]
        except:
            videos = jsonresponse

        video_data = videos["data"]
        if "next" in videos["paging"]:
            next = videos["paging"]["next"]
        else:
            next = False
        video_updated_time = False
        video_created_time = False
        for video_item in video_data:
            video_id = video_item["id"]
            if "title" in video_item:
                video_title = video_item["title"]
            else:
                video_title = ""
            if "description" in video_item:
                video_description = video_item['description']
            else:
                video_description = ""
            video_link = video_item['permalink_url']
            video_created_time = video_item["created_time"]
            video_updated_time = video_item["updated_time"]

            video_df = pd.DataFrame(
                [[video_id, video_title, video_description, video_created_time, video_updated_time, video_link, FANPAGE_ID]],
                columns=['video_id', 'title', 'description', 'created_time', 'updated_time', 'link_video', "shop_id"]
            )
            insert_to_db(df=video_df, table_name='video')

        if video_created_time and int(video_created_time[:4]) > int(os.getenv("THRESHOLD_YEAR")):
            logging.warning("Continue parse")
            if next:
                yield scrapy.Request(next, callback=self.parse)
