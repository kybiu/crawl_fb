import psycopg2
import os
from dotenv import load_dotenv
import pandas as pd
import psycopg2.extras as extras

load_dotenv()

HOST = os.environ.get("HOST")
PORT = os.environ.get("PORT")
DATABASE = os.environ.get("DATABASE")
USER = os.environ.get("USER")
PASSWORD = os.environ.get("PASSWORD")
SCHEMA = os.environ.get("SCHEMA")

param_dict = {
    "host": HOST,
    "port": PORT,
    "database": DATABASE,
    "user": USER,
    "password": PASSWORD
}


def connect_db():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**param_dict)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    print("Connection successful")
    return conn


def execute_custom_select_query(query):
    conn = connect_db()
    cursor = conn.cursor()
    try:
        cursor.execute(query)
        records = cursor.fetchall()
        col_names = [x.name for x in cursor.description]
        cursor.close()
        return records, col_names
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1


def select_all(table_name):
    if SCHEMA:
        table_name = SCHEMA + '.' + table_name
    conn = connect_db()

    # SQL quert to execute
    query = "select * from " + table_name
    cursor = conn.cursor()
    try:
        cursor.execute(query)
        records = cursor.fetchall()
        col_names = [x.name for x in cursor.description]
        cursor.close()
        return records, col_names
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1


def insert_to_db(df: pd.DataFrame, table_name):
    """
       Using psycopg2.extras.execute_values() to insert the dataframe
       """
    if SCHEMA:
        table_name = SCHEMA + '.' + table_name
    conn = connect_db()
    # Create a list of tupples from the dataframe values
    tuples = [tuple(x) for x in df.to_numpy()]
    # Comma-separated dataframe columns
    cols = ','.join(list(df.columns))

    # SQL quert to execute
    query = "INSERT INTO %s(%s) VALUES %%s" % (table_name, cols)
    cursor = conn.cursor()
    try:
        extras.execute_values(cursor, query, tuples)
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1
    print("execute_values() done")
    cursor.close()


def get_post_id_by_time_range(start, end, shop_id, table_name="post"):
    if SCHEMA:
        table_name = SCHEMA + '.' + table_name
    conn = connect_db()

    # SQL quert to execute
    # query = "select * from " + table_name
    # query = """
    # select post_id, sub_post_id
    # from {table_name}
    # where shop_id = {shop_id}
    # and created_time < {end} and created_time >= {start}
    # """
    query = """
    select post_id, sub_post_id
    from {table_name}
    where shop_id = {shop_id}
    and created_time < {end} and created_time >= {start}
    """
    query = query.format(table_name=table_name, shop_id=shop_id, end="'" + end + "'", start="'" + start + "'")
    cursor = conn.cursor()
    try:
        cursor.execute(query)
        records = cursor.fetchall()
        col_names = [x.name for x in cursor.description]
        cursor.close()
        return records, col_names
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1


def get_video_id_by_time_range(start, end, shop_id, table_name="video"):
    if SCHEMA:
        table_name = SCHEMA + '.' + table_name
    conn = connect_db()

    query = """
    select video_id
    from {table_name}
    where shop_id = {shop_id}
    and created_time < {end} and created_time >= {start}
    """
    query = query.format(table_name=table_name, shop_id=shop_id, end="'" + end + "'", start="'" + start + "'")
    cursor = conn.cursor()
    try:
        cursor.execute(query)
        records = cursor.fetchall()
        col_names = [x.name for x in cursor.description]
        cursor.close()
        return records, col_names
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1


def get_data_post(start, end, shop_id, table_name="post"):
    if SCHEMA:
        table_name = SCHEMA + '.' + table_name
    conn = connect_db()

    # SQL quert to execute
    # query = "select * from " + table_name
    # query = """
    # select post_id, sub_post_id
    # from {table_name}
    # where shop_id = {shop_id}
    # and created_time < {end} and created_time >= {start}
    # """
    query = """
    select *
    from {table_name}
    where shop_id = {shop_id}
    and created_time < {end} and created_time >= {start}
    """
    query = query.format(table_name=table_name, shop_id=shop_id, end="'" + end + "'", start="'" + start + "'")
    # query = query.format(table_name=table_name, shop_id=shop_id)
    cursor = conn.cursor()
    try:
        cursor.execute(query)
        records = cursor.fetchall()
        col_names = [x.name for x in cursor.description]
        cursor.close()
        return records, col_names
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1


if __name__ == '__main__':
    a = get_data_post(start='2020-01-01', end='2021-01-01', shop_id='1059845080701224', table_name="comment")
    df = pd.DataFrame(data=a[0], columns=a[1])
    df.to_csv("comment.csv", index=False, encoding='utf-8')
