import logging
import scrapy
import json
from datetime import date
from csv import DictWriter
import time
import datetime
import os
from dotenv import load_dotenv
from collections import Iterable
import pandas as pd
import sys
from fbcrawler.spiders.db_connector import insert_to_db

load_dotenv()

sys.path.append('.')
sys.path.append('../')

logging.basicConfig(
    filename='error_id.log',
    level=logging.CRITICAL,
    format='%(asctime)s %(module)s - %(funcName)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)

FANPAGE_ID = os.environ.get("FANPAGE_ID")
ACCESS_TOKEN = os.environ.get("ACCESS_TOKEN")
RETRY_TIMES = os.environ.get("RETRY_TIMES")
reaction_dict = {"NONE": 1, "LIKE": 2, "LOVE": 3, "WOW": 4, "HAHA": 5, "SORRY": 6, "ANGRY": 7, "CARE": 8, "SAD": 9}


def get_timestamp(timestamp: int, format: str):
    """

    :param timestamp:
    :param format: %Y-%m-%d %H:%M:%S
    :return:
    """
    readable_timestamp = datetime.datetime.utcfromtimestamp(timestamp).strftime(format)
    return readable_timestamp


def get_reaction_id(reaction_type: str):
    return reaction_dict[reaction_type]


def convert_time_to_correct_timezone(message_time: str):
    message_time = message_time.replace("T", " ")
    message_time = message_time.replace("+0000", " ")
    message_time = message_time.strip()
    message_time = datetime.datetime.strptime(message_time, "%Y-%m-%d %H:%M:%S")

    hours_added = datetime.timedelta(hours=7)
    message_time = message_time + hours_added
    message_time = str(message_time)
    return message_time


def update_post_reaction_table():
    pass


def readable_api_url(api_url: str):
    new_api_url = api_url.replace("%7B", "{{")
    new_api_url = new_api_url.replace("%7D", "}}")
    new_api_url = new_api_url.replace("%2C", ",")
    return new_api_url


class FbPostSpider(scrapy.Spider):
    name = 'fb_post_spider'

    post_api = "https://graph.facebook.com/v9.0/{fanpage_id}?fields=posts.limit(100){{id,message,created_time,updated_time,attachments,reactions{{id,name,username,type,link}},permalink_url,comments{{comment_count}}}}&access_token={token}"
    post_api = post_api.format(fanpage_id=FANPAGE_ID, token=ACCESS_TOKEN)

    conversation_timestamp_year = os.getenv("YEAR")

    def start_requests(self):
        logging.critical("Start request")
        url = self.post_api
        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        logging.critical("Parse")
        jsonresponse = json.loads(response.text)
        try:
            posts = jsonresponse["posts"]
        except:
            posts = jsonresponse

        post_data = posts["data"]
        if "next" in posts["paging"]:
            next = posts["paging"]["next"]
        else:
            next = False
        post_updated_time = False
        post_created_time = False
        for post_item in post_data:
            post_id = post_item["id"]
            try:
                post_message = post_item["message"]
            except:
                post_message = ""
            post_created_time = post_item["created_time"]
            post_created_time = convert_time_to_correct_timezone(post_created_time)
            post_updated_time = post_item["updated_time"]
            post_updated_time = convert_time_to_correct_timezone(post_updated_time)

            post_link = post_item["permalink_url"]
            if int(post_created_time[:4]) == int(os.getenv("YEAR")):
                sub_post_id = ""
                attachments_count = 0
                attachments_json = json.dumps([])
                if "attachments" in post_item:
                    attachments_data = post_item["attachments"]["data"]
                    attachments_list = []
                    for attachments in attachments_data:
                        if "subattachments" in attachments:
                            subattachments_data = attachments["subattachments"]["data"]
                            attachments_count += len(subattachments_data)
                            sub_post_id += ",".join([x["target"]["id"] for x in subattachments_data]) + ","
                            attachments_list.append(subattachments_data)
                    attachments_json = json.dumps(attachments_list)

                reaction_count = 0
                reaction_json = json.dumps([])
                if "reactions" in post_item:
                    reaction_data = post_item["reactions"]["data"]
                    reaction_count = len(reaction_data)
                    reaction_ids = list(map(get_reaction_id, [x["type"] for x in reaction_data]))
                    reaction_json = json.dumps(reaction_data)

                post_df = pd.DataFrame(
                    [[post_id, sub_post_id, post_message, post_created_time, post_updated_time, attachments_json, attachments_count, reaction_json, reaction_count, post_link, FANPAGE_ID]],
                    columns=['post_id', 'sub_post_id', 'post_message', 'created_time', 'updated_time', 'attachments', 'attachments_count', 'reaction', 'reaction_count', 'link_to_post', "shop_id"]
                )
                insert_to_db(df=post_df, table_name='post')

        if post_created_time and int(post_created_time[:4]) > int(os.getenv("THRESHOLD_YEAR")):
            logging.warning("Continue parse")
            if next:
                yield scrapy.Request(next, callback=self.parse)
