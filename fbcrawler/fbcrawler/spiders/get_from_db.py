# from fbcrawler.spiders.db_connector import execute_custom_select_query
from db_connector import execute_custom_select_query
import pandas as pd
import sys

sys.path.append('.')
sys.path.append('../')
shop_name_id = {
    1683641515264365: "son_la",
    102960858308636: "nam_dinh",
    265285793950950: "quang_binh",
    309755979916488: "bac_ninh",
    107998511126938: "quang_ngai",
    445110719297455: "da_nang",
    445359565985516: "viet_tri",
    451521568956913: "buon_ma_thuat",
    1399467683482483: "ha_tinh",
    100812468528861: "lao_cai",
    102300305043296: "dien_bien",
    101242387938975: "ninh_binh",
    104826111453319: "hoa_binh",
    110901244172555: "quang_tri",
    104591744610398: "hai_duong"
}


def get_comment_from_db(start, end, shop_id):
    query = """
    select * from fb_chatlog.comment
    where shop_id = {shop_id}
    and created_time >= {start}
    and created_time < {end}
    """
    query = query.format(start="'" + start + "'", end="'" + end + "'", shop_id=shop_id)
    records, col_names = execute_custom_select_query(query)
    df = pd.DataFrame(columns=col_names, data=records)
    df = df.drop_duplicates(subset=["comment_id"])
    df.to_csv("comment_jan.csv", index=False)


def get_chatlog_from_db(start, end, shop_id):
    query = """
    select * from fb_chatlog.raw_chatlog
    where shop_id = {shop_id}
    and created_time >= {start}
    and created_time < {end}
    """
    query = query.format(start="'" + start + "'", end="'" + end + "'", shop_id=shop_id)
    records, col_names = execute_custom_select_query(query)
    df = pd.DataFrame(columns=col_names, data=records)
    df = df.drop_duplicates(subset=["message_id"])
    df.to_csv("01_2021/" + shop_name_id[shop_id] + ".csv", index=False)


def get_comment_from_db():
    query = """
    select * from fb_chatlog.comment
    where parent_post_id in (
	select post_id from fb_chatlog.post
    where created_time >= '2021-01-01'
    and created_time < '2021-01-05' 
	)
    """
    # query = query.format(start="'" + start + "'", end="'" + end + "'", shop_id=shop_id)
    records, col_names = execute_custom_select_query(query)
    df = pd.DataFrame(columns=col_names, data=records)
    df = df.drop_duplicates(subset=["comment_id"])
    df.to_csv("comment_jan_bosung.csv", index=False)


# get_comment_from_db()

df1 = pd.read_csv("comment_jan.csv")
df2 = pd.read_csv("comment_jan_bosung.csv")
df = pd.concat([df1, df2])
df.to_csv("comment_jan_update.csv", index=False)
# get_comment_from_db(start='2021-01-01', end='2021-02-01', shop_id=1059845080701224)
# for shop_id in shop_name_id.keys():
#     get_chatlog_from_db(start='2021-01-01', end='2021-02-01', shop_id=shop_id)
