import ast
import pandas as pd
import time
import json


def read_chatlog_file():
    start_time = time.time()
    with open('fbcrawler/data/test/shop_gau_11.json', encoding='utf-8') as data_file:
        data = json.loads(data_file.read())
    chatlog_df = pd.DataFrame(data)
    print("Read file: " + str(time.time() - start_time))
    return chatlog_df


def split_and_export_chatlog_to_month(chatlog_df):
    month_data = {"01": [], "02": [], "03": [], "04": [], "05": [], "06": [], "07": [], "08": [], "09": [], "10": [],
                  "11": [], "12": []}
    for item_index in range(len(chatlog_df)):
        month = chatlog_df.at[item_index, "created_time"][5:7]
        month_data[month].append(chatlog_df.loc[item_index])

    for month_index, data_in_month in month_data.items():
        df = pd.DataFrame(data_in_month)

        for row in df.itertuples():
            index = row.Index
            created_time = row.created_time
            created_time_reformat = created_time[:10] + 'T' + created_time[11:] + "+0000"

            updated_time = row.updated_time
            updated_time_reformat = updated_time[:10] + 'T' + updated_time[11:] + "+0000"

            df.at[index, 'created_time'] = created_time_reformat
            df.at[index, 'updated_time'] = updated_time_reformat

        df.to_csv("/home/ducanh/pycharm_prj/crawl-fb-scrapy/fbcrawler/fbcrawler/data/test/test_" + str(month_index) + ".csv", index=False)
    return 0


def main():
    chatlog_df = read_chatlog_file()
    split_and_export_chatlog_to_month(chatlog_df)


if __name__ == '__main__':
    main()
